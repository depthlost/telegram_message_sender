#!/usr/bin/python3

import threading

from arguments import get_arguments
import telegram

args = get_arguments()

if len(args.user_ids) > 1:
    threads = []

    for user_id in args.user_ids:
        thread = threading.Thread(target=telegram.send_message, args=(args.bot_id, user_id, args.message, args.number_of_attemps, args.format))
        threads.append(thread)
        
        thread.start()

    for thread in threads:
        thread.join()
else:
    telegram.send_message(args.bot_id, args.user_ids[0], args.message, args.number_of_attemps, args.format)
