
import time

import requests

def send_message(bot_id, user_id, message, number_of_attemps, format=None):
    done = False

    data = {
        "chat_id": user_id,
        "text": message
    }

    if format:
        data["parse_mode"] = format

    while not done and number_of_attemps > 0:
        try:
            requests.post(f"https://api.telegram.org/bot{bot_id}/sendMessage", data=data)

            done = True
        except:
            number_of_attemps -= 1
            
            if number_of_attemps > 0:
                time.sleep(3)

    return done
