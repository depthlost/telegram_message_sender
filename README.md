# Telegram Message Sender

Simple program to send Telegram messages.

```
usage: run.py [-h] -b bot_id -u user_id [user_id ...] -m message
              [-n number_of_attemps] [-f format]

optional arguments:
  -h, --help            show this help message and exit
  -b bot_id, --bot bot_id
                        bot sender of the message
  -u user_id [user_id ...], --users user_id [user_id ...]
                        users receiving the message
  -m message, --message message
                        message to send
  -n number_of_attemps, --number-of-attemps number_of_attemps
                        number of attempts to send the message (default: 3)
  -f format, --format format
                        format for rendering the message. the available
                        options are: HTML, Markdown and MarkdownV2
```