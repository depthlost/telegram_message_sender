
import argparse

def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-b", "--bot", dest="bot_id", type=str, help="bot sender of the message", metavar="bot_id", required=True)
    parser.add_argument("-u", "--users", dest="user_ids", type=str, help="users receiving the message", metavar="user_id", nargs="+", required=True)
    parser.add_argument("-m", "--message", dest="message", type=str, help="message to send", metavar="message", required=True)
    parser.add_argument("-n", "--number-of-attemps", dest="number_of_attemps", type=int, help="number of attempts to send the message (default: 3)", metavar="number_of_attemps", default=3, required=False)
    parser.add_argument("-f", "--format", dest="format", type=str, help="format for rendering the message. the available options are: HTML, Markdown and MarkdownV2", metavar="format", choices=['HTML', 'Markdown', 'MarkdownV2'], required=False)

    return parser.parse_args()
